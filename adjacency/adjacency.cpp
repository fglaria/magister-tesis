// g++ -std=c++11 adjacency.cpp -o adjacency -O3 -DNDEBUG

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <set>

int main(int argc, char* argv[]) {

    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    // Path to files
    const std::string filePath(argv[1]);

    std::ifstream fileStream;
    std::string lineFile;

    uint64_t totalNodes = 0, totalArcs = 0;
    uint64_t lastNode = 1000; // Any value != 0 to compare with first node (0)

    // Open cliques file & create cliques vector
    fileStream.open(filePath.c_str(), std::ios::in);

    if(fileStream.is_open())
    {
        while(getline(fileStream, lineFile))
        {
            std::stringstream ssLine(lineFile);
            uint64_t firstNode, secondNode;
            
            ssLine >> firstNode;
            --firstNode;
            ssLine >> secondNode;
            --secondNode;

            if (lastNode != firstNode)
            {
                if(1 == firstNode - lastNode)
                {
                    std::cout << std::endl;
                }

                std::cout << firstNode << ": ";

                lastNode = firstNode;

                ++totalNodes;
            }

            std::cout << secondNode << " ";

            ++totalArcs;
        }

        std::cout << std::endl;
    }

    fileStream.close();

    std::cerr << "Total nodes " << totalNodes << std::endl;
    std::cerr << "Total arcs " << totalArcs << std::endl;


    return 0;
}