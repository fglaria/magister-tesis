// g++ -std=c++11 adjacencyGeneral.cpp -o adjacencyGeneral -O3 -DNDEBUG

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <set>

int main(int argc, char* argv[]) {

    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    // Path to files
    const std::string filePath(argv[1]);

    std::ifstream fileStream;
    std::string lineFile;

    std::map<uint32_t, std::set<uint32_t>> adjacencyList;
    uint32_t totalArcs = 0;

    // Open cliques file & create cliques vector
    fileStream.open(filePath.c_str(), std::ios::in);

    if(fileStream.is_open())
    {
        while(getline(fileStream, lineFile))
        {
            std::stringstream ssLine(lineFile);
            uint32_t firstNode, secondNode;

            ssLine >> firstNode;
            --firstNode;
            ssLine >> secondNode;
            --secondNode;

            // // Just for display
            // if(lastNode != firstNode && firstNode%1000 == 0)
            // {
            //     std::cerr << "Node " << firstNode << std::endl;
            //     lastNode = firstNode;
            // }

            if (adjacencyList[firstNode].insert(secondNode).second)
            {
                ++totalArcs;
            }
            if (adjacencyList[secondNode].insert(firstNode).second)
            {
                ++totalArcs;
            }

            // // Just for testing
            // if(30 < totalArcs)
            // {
            //     break;
            // }
        }
    }

    fileStream.close();


    int64_t lastNode = -1;
    uint32_t totalNodes = adjacencyList.size();

    for (const auto &nodeList : adjacencyList)
    {
        const uint32_t &rootNode = nodeList.first;

        // If nodes didn't had any neighbors, should be empty on output set
        uint32_t nodeDiff = rootNode - lastNode;
        if(nodeDiff > 1)
        {
            for (uint32_t interNode = lastNode + 1; interNode < rootNode; ++interNode)
            {
                std::cout << interNode << ": " << std::endl;
            }

            totalNodes += nodeDiff - 1;
        }

        // Output nodes with neighbors
        std::cout << rootNode << ": ";
        for (const auto &node : nodeList.second)
        {
            std::cout << node << " ";
        }
        std::cout << std::endl;


        lastNode = rootNode;
    }


    std::cerr << "Total nodes " << totalNodes << std::endl;
    std::cerr << "Total arcs " << totalArcs << std::endl;


    return 0;
}