// g++ -std=c++11 jumpingParallel.cpp solution.cpp -o jumpingParallel -msse2 -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG
// binary_functions.cpp (add if binary library)

#include <set>
#include <map>
#include <fstream>
#include <sstream>
#include <string>
#include <sys/times.h>
#include <chrono>

#include "vectorclass/vectorclass.h"

#include <sdsl/int_vector.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/util.hpp>
#include <sdsl/rank_support.hpp>
#include <sdsl/select_support.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/suffix_arrays.hpp>

#include "solution.h"

void JumpingParallel (sdsl::wm_int<sdsl::rrr_vector<63>> &x_wm, sdsl::rrr_vector<63> &rrrB1,
    sdsl::wm_int<sdsl::rrr_vector<63>> &y3_wm, unsigned char *B4, std::set<uint32_t> searchedNodes,
    std::map<uint32_t, std::set<uint32_t>> &allNeighbors)
{
    // SDSL variables for B1
    sdsl::rrr_vector<63>::rank_1_type rrrB1_rank(&rrrB1);
    sdsl::rrr_vector<63>::select_1_type rrrB1_sel(&rrrB1);

    std::map<uint32_t, std::map<uint32_t, uint32_t>> xPartitionsIndexes;

    // Define and initialize unsigned char vectors for masks
    Vec16uc masksPossibleNeighbors = 0;
    Vec16uc masksSearchedNode = 0;

    // Max size of vectors
    const uint32_t maxSizeVector = sizeof(masksPossibleNeighbors);

    // Number of masks in Vectors
    uint8_t howManyMasksInVectors = 0;

    // Array of current X & compared
    uint32_t currentX[maxSizeVector];
    uint32_t comparedX[maxSizeVector];

    // For every node to search neigbors, search them
    for (auto searchedNode : searchedNodes)
    {
        // How many times is there a searched Node in X
        const uint32_t howManyX = x_wm.rank(x_wm.size(), searchedNode);

        // Let's get every partition with an X, and search for it's neighbors
        for (uint32_t iX = 1; iX <= howManyX; ++iX)
        {
            // Index of iX's ocurrence in X
            const uint32_t xIndex = x_wm.select(iX, searchedNode);
            // Number of 1s in B1 until xIndex, indicates current partition
            const uint32_t partition = rrrB1_rank(xIndex + 1);

            // Index of 1 in B1 of this partition
            const uint32_t currentB1Index = rrrB1_sel(partition);
            // Index of 1 in B1 of next partition
            const uint32_t nextB1Index = rrrB1_sel(partition + 1);
            // Number of elements in partition
            const uint32_t elements = nextB1Index - currentB1Index;


            // If partitions hasn't been visited, lets add some content to it
            if (0 == xPartitionsIndexes.count(partition))
            {
                for (int iXP = currentB1Index; iXP < nextB1Index; ++iXP)
                {
                    xPartitionsIndexes[partition][iXP] = x_wm[iXP];
                }
            }


            // Index of X inside partition (reference to currentB1Index)
            const uint32_t xPartitionIndex = xIndex - currentB1Index;


            // Number of bytes to skip in B4 per partition
            const uint32_t currentY = y3_wm[partition-1];
            // Number of bytes to skip in B4
            const uint32_t nextY = y3_wm[partition];
            // Number of bytes to read
            const uint32_t bytesInB4 = nextY - currentY;
            // Number of bytes per mask
            const uint32_t bytesPerMask = bytesInB4/elements;


            // For every element in partition, compare to searchedNode and check if neighbors
            for (uint32_t iPartition = 0; iPartition < elements; ++iPartition)
            {
                // Get current X to compare
                const uint32_t actualX = xPartitionsIndexes[partition][currentB1Index + iPartition];

                // If it's the same searchedNode, skip it
                if(actualX == searchedNode)
                {
                    continue;
                }

                // If just one byte, they're neighbors
                if(1 == bytesInB4)
                {
                    allNeighbors[searchedNode].insert(actualX);
                    continue;
                }


                // Count bytes copied to vector
                uint32_t bytesToCheck = 0;

                // While the are bytes to copy of same X
                while (bytesToCheck != bytesPerMask)
                {
                    // std::cout << "bytesToCheck " << bytesToCheck << std::endl;

                    // Byte of Mask of searchedNode
                    const unsigned char maskOfSearchedNode = B4[currentY + xPartitionIndex*bytesPerMask + bytesToCheck];

                    // Byte of mask of Possible Neighbor
                    const unsigned char maskOfPossibleNeighbor = B4[currentY + iPartition*bytesPerMask + bytesToCheck];


                    // Put masks on vectors
                    masksPossibleNeighbors.insert(howManyMasksInVectors, maskOfPossibleNeighbor);
                    masksSearchedNode.insert(howManyMasksInVectors, maskOfSearchedNode);

                    // Store X value
                    currentX[howManyMasksInVectors] = searchedNode;
                    comparedX[howManyMasksInVectors] = actualX;

                    // Plus one to how many bytes are in vectors
                    howManyMasksInVectors += 1;


                    // If vectors are full
                    if (maxSizeVector == howManyMasksInVectors)
                    {
                        solution::evaluateNeighbors(masksPossibleNeighbors, masksSearchedNode,
                            currentX, comparedX, allNeighbors);

                        // Clean vectors
                        masksPossibleNeighbors = 0;
                        masksSearchedNode = 0;

                        howManyMasksInVectors = 0;
                    }

                    // Next byte to copy mask
                    bytesToCheck += 1;
                }
            }
        }
    }

    solution::evaluateNeighbors(masksPossibleNeighbors, masksSearchedNode,
        currentX, comparedX, allNeighbors);
}

int main(int argc, char* argv[]) {

    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    // Path to files
    const std::string path(argv[1]);

    // Iterations is 20, unless it's given
    const uint32_t iterations = argv[2] ? atoi(argv[2]) : 20;


    // Time to acumulate
    double t_acum = 0.0;

    sdsl::wm_int<sdsl::rrr_vector<63>> x_wm;
    sdsl::rrr_vector<63> rrrB1;
    sdsl::wm_int<sdsl::rrr_vector<63>> y3_wm;
    unsigned char *B4 = solution::readData(path, x_wm, rrrB1, y3_wm);



    // Path to X seq file
    std::string sX = path+".seqX";
    // stream to X file
    std::ifstream seqXfile (sX.c_str());

    // Reading X files
    if (getline(seqXfile, sX))
    {
        seqXfile.close();
    }
    else
    {
        std::cout << "Unable to open file seqXfile" << std::endl;
        return -1;
    }

    // String to Vector of int
    std::stringstream ssX(sX);
    // Set of nodes to look their neighbors
    std::set<uint32_t> searchedNodes;
    // To store node values
    uint32_t number;

    // For every node, extract value and add it to set
    while (ssX >> number)
    {
        searchedNodes.insert(number);
    }



    // Generate original graph iterations times to have an average
    for (int i = 1; i <= iterations; ++i)
    {
        // Get all nodes of graph
        std::map<uint32_t, std::set<uint32_t>> allGraph;

        std::chrono::high_resolution_clock::time_point st = std::chrono::high_resolution_clock::now();

        JumpingParallel(x_wm, rrrB1, y3_wm, B4, searchedNodes, allGraph);

        std::chrono::high_resolution_clock::time_point et = std::chrono::high_resolution_clock::now();

        auto duration = std::chrono::duration_cast<std::chrono::milliseconds> (et-st).count();


        std::ofstream allNodesFile(path + ".allNodesJP", std::ofstream::out | std::ofstream::trunc);


        uint32_t alledges = 0;
        // Write nodes to file
        for (auto mapped : allGraph)
        {
            allNodesFile << mapped.first << ": ";
            for (auto neighbor : mapped.second)
            {
                allNodesFile << neighbor << " ";

                alledges += (mapped.first != neighbor) ? 1 : 0;
            }
            allNodesFile << std::endl;
        }

        allNodesFile.close();


        t_acum += duration;

        std::cout << "Loop " << i << " " << solution::time_representation(duration) << " Alledges " << alledges << std::endl;
    }

    double average = t_acum/iterations;

    std::cout << "Promedio " << solution::time_representation(average) << std::endl;

    return 0;
}
