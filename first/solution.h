#ifndef TOPICOS_SOLUTION
#define TOPICOS_SOLUTION

#include <iostream>
#include <fstream>
#include <set>
#include <map>
#include "vectorclass/vectorclass.h"

#include <sdsl/int_vector.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/util.hpp>
#include <sdsl/rank_support.hpp>
#include <sdsl/select_support.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/suffix_arrays.hpp>

// #include "binary_functions.h"

namespace solution
{
    void evaluateNeighbors(const Vec16uc& possible, const Vec16uc& searched,
        uint32_t* currentX, uint32_t* comparedX,
        std::map<uint32_t, std::set<uint32_t>>& allNeighbors
    );

    unsigned char *readData(
        const std::string path,
        sdsl::wm_int<sdsl::rrr_vector<63>> &x_wt,
        sdsl::rrr_vector<63> &rrrB1,
        sdsl::wm_int<sdsl::rrr_vector<63>> &y3_wt
    );

    void readDataB4(
        const std::string path,
        sdsl::wm_int<sdsl::rrr_vector<63>> &x_wt,
        sdsl::rrr_vector<63> &rrrB1,
        sdsl::wt_hutu<sdsl::rrr_vector<63>> &b4_wt,
        sdsl::wm_int<sdsl::rrr_vector<63>> &y3_wt
    );

    std::string time_representation(double t);
}

#endif
