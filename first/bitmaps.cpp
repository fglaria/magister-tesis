// g++ -std=c++11 bitmaps.cpp -o bitmaps -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64

#include <stdio.h>
#include <stdlib.h>

#include <cstdlib>
#include <map>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <iostream>
#include <sdsl/vectors.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/wavelet_trees.hpp>

using namespace std;
using namespace sdsl;

double createSeq(int wt_type, string outname, string inname ){
    double ssize = 0.0;
    string wtfile;

    cout << " inname " << inname << " outname " << wtfile << endl;

    if (wt_type == 0) {
        wt_int<rrr_vector<63>> wtint;
        construct(wtint, inname.c_str(), 4);

        cout << "wtint.size()=" << wtint.size() <<
            " in mega bytes = " << size_in_mega_bytes(wtint) << endl;
        cout << "wtint.sigma =" << wtint.sigma << endl;

        ssize = size_in_mega_bytes(wtint)*1024*1024*8;
        wtfile = outname + "-wtint.sdsl";
        store_to_file(wtint,wtfile.c_str());
    }
    else if (wt_type == 1)
    {
        wm_int<rrr_vector<63>> wmint;
        construct(wmint, inname.c_str(), 4);

        cout << "inname " << inname << " wmint.size()=" << wmint.size() <<
            " in mega bytes = " << size_in_mega_bytes(wmint) << endl;

        ssize = size_in_mega_bytes(wmint)*1024*1024*8;
        cout << "wmint.sigma =" << wmint.sigma << endl;

        wtfile = outname + "-wmint.sdsl";
        store_to_file(wmint,wtfile.c_str());
    }
}

double createBitmap(int bitmap_type, bit_vector &b, string outname ) {
    double bsize = 0.0;

    if (bitmap_type == 0)
    {
        rrr_vector<63> rrrb(b);
        cout << "rrr bitmap size " << size_in_mega_bytes(rrrb) << endl;
        cout << "rrr bitmap size in bits " << size_in_bytes(rrrb)*8 << endl;

        bsize = size_in_mega_bytes(rrrb)*1024*1024*8;
        outname = outname + "-rrr-64.sdsl";
        cout << outname << endl;

        store_to_file(rrrb, outname.c_str());
    }
    else if (bitmap_type == 1)
    {
        sd_vector<> sdb(b);
        cout << "sdb bitmap size " << size_in_mega_bytes(sdb) << endl;

        bsize = size_in_mega_bytes(sdb)*1024*1024*8;
        outname = outname + "-sdb.sdsl";
        store_to_file(sdb, outname.c_str());
    }

    return bsize;
}


int main(int argc, char ** argv) {

    if(3 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA bitmap (0: rrr, 1:sdb)" << std::endl;
        return -1;
    }


    std::string path(argv[1]);
    int bitmap_type = atoi(argv[2]);

    // // Path to files
    // string sXbin = path+".seqX.bin";
    std::string sB1 = path + ".B";
    // string sB3bin = path+".B3.bin";
    // string sY2bin = path+".seqY2.bin";

    std::ifstream Bfile (sB1.c_str());

    // // SDSL variables to X, B1, B3, Y2
    // sdsl::wm_int<sdsl::rrr_vector<63>> x_wt;
    // std::ifstream b3file (sB3bin, std::ios::in | std::ios::binary | std::ios::ate);
    // sdsl::wm_int<sdsl::rrr_vector<63>> y2_wt;
    std::vector<uint32_t> B1;

    // // X, B3, Y2
    // sdsl::construct(x_wt, sXbin.c_str(), 4);
    // sdsl::construct(y2_wt, sY2bin.c_str(), 4);

    // B1
    if (getline (Bfile, sB1))
    {
        Bfile.close();
    }
    else
    {
        std::cout << "Unable to open file Bfile" << std::endl;
        return -1;
    }

    std::stringstream ssB1(sB1);

    uint32_t number;
    while (ssB1 >> number)
    {
        B1.push_back(number);
    }

    size_t Blen = B1.size();
    cout << "Blen " << Blen << endl;

    bit_vector B = bit_vector(Blen, 0);
    for (size_t i=0; i <= Blen; i++) {
        if(B1[i] == 1)
        {
            B[i] = 1;
        }
    }


    // double bcsize = createBitmap(bitmap_type, bc, bcFile);
    double Bsize = createBitmap(bitmap_type, B, path + ".B");

    string inseqfile = path + ".seqX.bin";
    //double seqXsize = createSeq(wt_type, outdir + wtfile, inseqfile);
    double Xsize = createSeq(1, path + ".X", inseqfile);

    string inseqfileY = path + ".seqY3.bin";
    //double seqXsize = createSeq(wt_type, outdir + wtfile, inseqfile);
    double Ysize = createSeq(1, path + ".Y3", inseqfileY);
}
