import sys
import os

if 2 > len(sys.argv):
    print "Modo de uso: " + sys.argv[0] + " RUTA"
    sys.exit()

# Path to file
path = sys.argv[1]

extensions = (
    "seqX.bin",
    "seqX.bin-wm_int.sdsl",
    "B-rrr-64.sdsl",
    "Bc-rrr-64.sdsl",
    "seqBC.bin",
    # "seqBC-wmint.sdsl",
    "seqY3.bin-wm_int.sdsl"
)

for extension in extensions:
    statinfo = os.stat(path + '.' + extension)
    print extension + " size " + str(statinfo.st_size*8) + " bits"
