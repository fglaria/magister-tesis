#!/bin/bash
# bash runAll.sh dblp-2011/dblp-2011 1

if [ "$3" == "compile" ]; then
    echo "g++ -std=c++11 onlySDSL.cpp solution.cpp -o onlySDSL -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"
    eval "g++ -std=c++11 onlySDSL.cpp solution.cpp -o onlySDSL -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"

    echo "g++ -std=c++11 secuencial.cpp solution.cpp -o secuencial -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"
    eval "g++ -std=c++11 secuencial.cpp solution.cpp -o secuencial -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"

    echo "g++ -std=c++11 compSecuencial.cpp solution.cpp -o compSecuencial -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"
    eval "g++ -std=c++11 compSecuencial.cpp solution.cpp -o compSecuencial -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"

    echo "g++ -std=c++11 parallel.cpp solution.cpp -o parallel -msse2 -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"
    eval "g++ -std=c++11 parallel.cpp solution.cpp -o parallel -msse2 -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"

    echo "g++ -std=c++11 pragmaMethod.cpp solution.cpp -o pragmaMethod -fopenmp -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"
    eval "g++ -std=c++11 pragmaMethod.cpp solution.cpp -o pragmaMethod -fopenmp -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"

    echo "g++ -std=c++11 jumpingMethod.cpp solution.cpp -o jumpingMethod -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"
    eval "g++ -std=c++11 jumpingMethod.cpp solution.cpp -o jumpingMethod -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"

    echo "g++ -std=c++11 jumpingParallel.cpp solution.cpp -o jumpingParallel -msse2 -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"
    eval "g++ -std=c++11 jumpingParallel.cpp solution.cpp -o jumpingParallel -msse2 -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"

    echo "g++ -std=c++11 mapUnordered.cpp solution.cpp -o mapUnordered -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"
    eval "g++ -std=c++11 mapUnordered.cpp solution.cpp -o mapUnordered -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"

    echo "g++ -std=c++11 allUnordered.cpp solution.cpp -o allUnordered -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"
    eval "g++ -std=c++11 allUnordered.cpp solution.cpp -o allUnordered -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"

    echo "g++ -std=c++11 unorderedPragma.cpp solution.cpp -o unorderedPragma -fopenmp -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"
    eval "g++ -std=c++11 unorderedPragma.cpp solution.cpp -o unorderedPragma -fopenmp -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG"
fi

if [ "$1" == "" ] || [ "$2" == "" ]; then
    echo "USAGE: $0 path iterations [compile]"
else
    path="$1"
    iter="$2"

    printf "\n./onlySDSL $1 $2\n"
    eval "./onlySDSL $1 $2"

    printf "\n./secuencial $1 $2\n"
    eval "./secuencial $1 $2"

    printf "\n./compSecuencial $1 $2\n"
    eval "./compSecuencial $1 $2"

    printf "\n./parallel $1 $2\n"
    eval "./parallel $1 $2"

    printf "\n./pragmaMethod $1 $2\n"
    eval "./pragmaMethod $1 $2"

    printf "\n./jumpingMethod $1 $2\n"
    eval "./jumpingMethod $1 $2"

    printf "\n./jumpingParallel $1 $2\n"
    eval "./jumpingParallel $1 $2"

    printf "\n./mapUnordered $1 $2\n"
    eval "./mapUnordered $1 $2"

    printf "\n./allUnordered $1 $2\n"
    eval "./allUnordered $1 $2"

    printf "\n./unorderedPragma $1 $2\n"
    eval "./unorderedPragma $1 $2"
fi
