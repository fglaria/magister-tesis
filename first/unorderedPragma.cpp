// g++ -std=c++11 unorderedPragma.cpp solution.cpp -o unorderedPragma -fopenmp -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG

#include <unordered_set>
#include <unordered_map>
#include <fstream>
#include <sstream>
#include <string>
#include <sys/times.h>
#include <chrono>

#include <sdsl/int_vector.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/util.hpp>
#include <sdsl/rank_support.hpp>
#include <sdsl/select_support.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/suffix_arrays.hpp>

#include "solution.h"


void unorderedPragma(sdsl::wm_int<sdsl::rrr_vector<63>> &x_wm, sdsl::rrr_vector<63> &rrrB1,
    sdsl::wm_int<sdsl::rrr_vector<63>> &y3_wm, unsigned char *B4,
    std::unordered_map<uint32_t, std::unordered_set<uint32_t>> &allNeighbors,
    std::unordered_set<uint32_t> allNodes)
{
    // SDSL variables for B1
    sdsl::rrr_vector<63>::rank_1_type rrrB1_rank(&rrrB1);
    sdsl::rrr_vector<63>::select_1_type rrrB1_sel(&rrrB1);

    std::unordered_map<uint32_t, std::unordered_set<uint32_t>> allN;

    for(auto node : allNodes)
    {
        allNeighbors[node] = std::unordered_set<uint32_t>();
        allN[node] = std::unordered_set<uint32_t>();
    }

    // How many partitions for this graph
    const uint32_t howManyPartitions = rrrB1_rank(rrrB1.size());

    // For every partition, let's find neighbors
    #pragma omp parallel shared(allNeighbors) private(allN)
    {
        #pragma omp for
        for (uint32_t partition = 1; partition < howManyPartitions; ++partition)
        {
            uint32_t currentY = y3_wm[partition-1];
            uint32_t currentB1Index = rrrB1_sel(partition);
            uint32_t nextB1Index = rrrB1_sel(partition + 1);
            const uint32_t elements = nextB1Index - currentB1Index;

            // Update Y values, and get bytes on partition and per element
            uint32_t nextY = y3_wm[partition];
            const uint32_t bytesInB4 = nextY - currentY;
            const uint32_t bytesPerMask = bytesInB4/elements;

            // Temporary array for storing elements in partition
            uint32_t arrayX[elements];

            // Copy every element to temporary arrayX
            for (uint32_t iElement = 0; iElement < elements; ++iElement)
            {
                arrayX[iElement] = x_wm[currentB1Index + iElement];
            }

            // For every element in partition, let's get their neighbors
            for (uint32_t iElement = 0; iElement < elements; ++iElement)
            {
                // If just one byte on B4, it means they're all neighbors
                // (should be one bit per element; new format translate this to one byte per partition)
                if(1 == bytesInB4)
                {
                    for(uint32_t xx = 0; xx < elements; ++xx)
                    {
                        allN[arrayX[xx]].insert(arrayX, arrayX + elements);
                    }

                    continue;
                }

                // Load current element to find neighbors
                uint32_t currentX = arrayX[iElement];

                // If more than one byte per partition, let's search for some neighbors
                //#pragma omp parallel for schedule(guided) private(currentX)
                for(uint32_t iNextElement = iElement+1; iNextElement < elements; ++iNextElement)
                {
                    // Count how many bytes are checked, and store possible neighbor
                    uint32_t bytesChecked = 0;
                    const uint32_t possibleX = arrayX[iNextElement];
                    // While there are byte to check
                    while (bytesChecked != bytesPerMask)
                    {
                        // Get byte mask to check neighbor
                        const unsigned char maskOfSearchedNode = B4[currentY + iElement*bytesPerMask + bytesChecked];
                        const unsigned char maskOfPossibleNeighbor = B4[currentY + iNextElement*bytesPerMask + bytesChecked];
                        // Compare masks
                        const unsigned char maskResult = maskOfPossibleNeighbor & maskOfSearchedNode;

                        // If result is anything but zero, they're neighbors
                        // List them, then break while
                        if (0 != maskResult)
                        {
                            allN[currentX].insert(possibleX);
                            allN[possibleX].insert(currentX);
                            break;
                        }
                        // Next byte to copy mask
                        bytesChecked += 1;
                    }
                }
            }
        }

        #pragma omp critical
        {
            for(auto node : allNodes){
                allNeighbors[node].insert(allN[node].begin(), allN[node].end());
            }
        }
    }
}



int main(int argc, char* argv[]) {

    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    // Path to files
    const std::string path(argv[1]);

    // Iterations is 20, unless it's given
    const uint32_t iterations = argv[2] ? atoi(argv[2]) : 20;


    // Time to acumulate
    double t_acum = 0.0;

    sdsl::wm_int<sdsl::rrr_vector<63>> x_wm;
    sdsl::rrr_vector<63> rrrB1;
    sdsl::wm_int<sdsl::rrr_vector<63>> y3_wm;
    unsigned char *B4 = solution::readData(path, x_wm, rrrB1, y3_wm);


    // Path to X seq file
    std::string sX = path+".seqX";
    // stream to X file
    std::ifstream seqXfile (sX.c_str());

    // Reading X files
    if (getline(seqXfile, sX))
    {
        seqXfile.close();
    }
    else
    {
        std::cout << "Unable to open file seqXfile" << std::endl;
        return -1;
    }

    // String to Vector of int
    std::stringstream ssX(sX);
    // Set of nodes to look their neighbors
    std::unordered_set<uint32_t> searchedNodes;
    // To store node values
    uint32_t number;

    // For every node, extract value and add it to set
    while (ssX >> number)
    {
        searchedNodes.insert(number);
    }


    // Generate original graph iterations times to have an average
    for (int i = 1; i <= iterations; ++i)
    {
        // Get all nodes of graph
        std::unordered_map<uint32_t, std::unordered_set<uint32_t>> allGraph;

        std::chrono::high_resolution_clock::time_point st = std::chrono::high_resolution_clock::now();

        unorderedPragma(x_wm, rrrB1, y3_wm, B4, allGraph, searchedNodes);

        std::chrono::high_resolution_clock::time_point et = std::chrono::high_resolution_clock::now();

        auto duration = std::chrono::duration_cast<std::chrono::milliseconds> (et-st).count();


        std::ofstream allNodesFile(path + ".allNodesUP", std::ofstream::out | std::ofstream::trunc);

        uint32_t alledges = 0;
        // Write nodes to file
        for (auto mapped : allGraph)
        {
            allNodesFile << mapped.first << ": ";
            for (auto neighbor : mapped.second)
            {
                allNodesFile << neighbor << " ";

                alledges += (mapped.first != neighbor) ? 1 : 0;
            }
            allNodesFile << std::endl;
        }

        allNodesFile.close();


        t_acum += duration;

        std::cout << "Loop " << i << " " << solution::time_representation(duration) << " Alledges " << alledges << std::endl;
    }

    double average = t_acum/iterations;

    std::cout << "Promedio " << solution::time_representation(average) << std::endl;

    return 0;
}
