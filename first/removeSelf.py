import sys

if 2 > len(sys.argv):
    print "Modo de uso: " + sys.argv[0] + " RUTA"
    sys.exit()

# Path to file
path = sys.argv[1]

f = open(path, 'r')
allLines = f.readlines()
f.close()

for index, line in enumerate(allLines):
    line = line.split(':')

    node = line[0]
    neighbors = line[1].strip().split(' ')

    if node in neighbors:
        neighbors.remove(node)

    allLines[index] = node + ": " + " ".join(neighbors) + "\n"


f = open(path, 'w')
f.writelines(allLines)
f.close()