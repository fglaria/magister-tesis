#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <set>

#include "Shingles.hpp"

using namespace std;

/*
template<typename A, typename B>
pair<B,A> flip_pair(const pair<A,B> &p)
{
    return pair<B,A>(p.second, p.first);
}

template<typename A, typename B>
multimap<B,A> flip_map(const map<A,B> &src)
{
    //multimap<B,A> dst;
    multimap<A,B> dst;
    transform(src.begin(), src.end(), inserter(dst, dst.begin()),
                   flip_pair<A,B>);
    return dst;
}
*/

template <typename A, typename B>
multimap<B,A> flip_map(map<A,B> &src) {
    multimap<B,A> dst;
    typename map<A,B>::iterator it;
    for(it = src.begin(); it != src.end(); it++){
        dst.insert(pair<B,A>(it -> second, it -> first));
    }
    return dst;
}

typedef vector< vector<int > > Sequences;

vector<int> split(string line, string delims){
   string::size_type bi, ei;
   vector<int> words;
   bi = line.find_first_not_of(delims);
   while(bi != string::npos) {
        ei = line.find_first_of(delims, bi);
        if(ei == string::npos)
                ei = line.length();
                string sv = line.substr(bi, ei-bi);
                int fv = atof(sv.c_str());
                words.push_back(fv);
                bi = line.find_first_not_of(delims, ei);
        }
  return words;
}


void
readSequencesFromFile(const char* fileName, map<int, vector<int> > &nodeClique, map<int,int> &cliqueUsed) {

    map<int, vector<int> >::iterator mit;
    int cliqueNumber = 0;
    ifstream infile(fileName);
    if (infile) {
        string line;
        while (getline(infile, line)) {
            if (line.empty())
                continue;
	    vector<int> items = split(line," ");
            for(unsigned int i=0; i<items.size(); i++){
		mit = nodeClique.find(items[i]);
		vector<int> nuevo;
		if(mit == nodeClique.end()){
			nuevo.push_back(cliqueNumber);
		} else {
			nuevo = mit->second;	
			nuevo.push_back(cliqueNumber);
		}
		//cout<<" nuevo size "<<nuevo.size()<<endl;
		nodeClique[items[i]] = nuevo;
	    } 
	    cliqueUsed[cliqueNumber] = 0;
	    cliqueNumber++;
        }
    }
}

void printVector (vector<Shingles::Signature> v){
	for(unsigned int i=0; i<v.size(); i++)
		cout<<v[i]<<" ";
	cout<<endl;
}

void printSet (set<int> s){
	set<int>::iterator sit;
	for(sit=s.begin(); sit!=s.end(); sit++){
		cout<<*sit<<" ";
	}
	cout<<endl;
}

void printMap (map<int, int> m){
	map<int, int>::iterator mit;
	for(mit = m.begin(); mit != m.end(); mit++){
		cout<<mit->first<<" "<<mit->second<<endl;
	}
}

void printBc (map<int, set<int> > m){
	map<int, set<int> >::iterator mit;
	set<int>::iterator sit;
	for(mit = m.begin(); mit != m.end(); mit++){
		set<int> s = mit->second;
		cout<<mit->first<<": ";
		printSet(s);
	}
}

void appendSeqX(set<int> nodes, ofstream &seq, ofstream &seqbin){
	set<int>::iterator sit;
 	unsigned int *buffer = new unsigned int[nodes.size()];
	int i=0;
	for(sit = nodes.begin(); sit != nodes.end(); sit++){
		seq<<*sit<<" ";
		buffer[i] = *sit;
		i++;
	}
	seqbin.write((char *)&*buffer,(nodes.size())*sizeof(unsigned int));
	delete[] buffer;
}

void appendBc(map<int, set<int> > mapNodeCliques, ofstream &bitmap, int numberCliques, set<int> nodes){
	map<int, set<int> >::iterator mit;
	set<int>::iterator sit, sit1;
	if(mapNodeCliques.size() != nodes.size()){
		cerr<<" error: numberNodes in Seq differs from numberNodes in mapcliques ";
		cerr<<nodes.size()<<" map size "<<mapNodeCliques.size()<<endl;
		exit(1);
	}
	unsigned int totalbits = numberCliques * nodes.size();
	int *bits = new int[totalbits];
	for(unsigned int i=0; i<totalbits; i++)
		bits[i] = 0;

	//for(unsigned int i=0; i<totalbits; i++)
	//		cout<<"bits["<<i<<"] = "<<bits[i]<<endl;

	int j=0;
	for(sit = nodes.begin(); sit != nodes.end(); sit++){ // foreach node there should be numberClusters bits
	        set<int> cliqueids = mapNodeCliques[*sit];
	        for(sit1 = cliqueids.begin(); sit1 != cliqueids.end(); sit1++){
			bits[j+*sit1] = 1;
	        }
		j=j+numberCliques;
	}
	for(unsigned int i=0; i<totalbits; i++)
		bitmap<<bits[i]<<" ";

	delete []bits;
}

void buildBitmapB( vector<int> numberCs, string outdir){
    string Bfilename = outdir + ".B";
    ofstream B;
    B.open(Bfilename.c_str(), std::ofstream::out);
    vector<int> Bitmap;
    Bitmap.push_back(1); // first 1 in Bitmap
    for(unsigned int i=0; i<numberCs.size(); i++){
	//for(int j=0; j<numberCs[i]; j++)
	for(int j=1; j<numberCs[i]; j++)
			Bitmap.push_back(0);
	Bitmap.push_back(1);
    }
    //cout<<"Bitmap"<<endl;
    for(unsigned int i=0; i<Bitmap.size(); i++){
	//cout<<Bitmap[i]<<" ";
	B<<Bitmap[i]<<" ";
    }
    //cout<<endl;
    B.close();
}

int buildSeqNumCliques(vector<int> numCliquesInCluster, string outdir){
    string seqbcfilename = outdir + ".seqBC";
    string seqbcfilenamebin = outdir + ".seqBC.bin";
    ofstream seqbc,seqbcbin;
    seqbc.open(seqbcfilename.c_str(), std::ofstream::out);
    seqbcbin.open(seqbcfilenamebin.c_str(), ofstream::binary);
    unsigned int *buffer = new unsigned int[numCliquesInCluster.size()];
    int totalCliques = 0;
    for(unsigned int i=0; i< numCliquesInCluster.size(); i++){
		seqbc<<numCliquesInCluster[i]<<" ";
		buffer[i] = numCliquesInCluster[i];
		totalCliques += numCliquesInCluster[i];
    }
    seqbcbin.write((char *)&*buffer,(numCliquesInCluster.size())*sizeof(unsigned int));
    delete[] buffer;
    seqbc.close();
    seqbcbin.close();
    //cout<<"totalCliques "<<totalCliques<<endl;
    return totalCliques;
}


int
main(int argc, char **argv) {
    if (argc != 3) {
        cerr << "error: missing argument: "<<argv[0]<<" SEQUENCES_FILE out_dir" << endl;
        return 1;
    }

    //Sequences sequences = readSequencesFromFile(argv[1]);
    //int nShingles = atoi(argv[2]);
    //int numSigs = atoi(argv[3]);
    //string outdir = string(argv[4]);
    string outdir = string(argv[2]);
/*
    string seqfilename = outdir + ".seqX";
    string seqfilenamebin = outdir + ".seqX.bin";
    ofstream seq,seqbin;
    seq.open(seqfilename.c_str(), std::ofstream::out);
    seqbin.open(seqfilenamebin.c_str(), ofstream::binary);
    string bcfilename = outdir + ".Bc";
    ofstream bc;
    bc.open(bcfilename.c_str(), std::ofstream::out);
*/

    // Build clusters
    //map< vector< Shingles::Signature >, vector<int> > clustersPos;
    //map< vector< Shingles::Signature >, int > clustersSizes;
    //Shingles shingle(nShingles, numSigs);

    map<int, vector<int> > nodeClique;
    map<int, vector<int> >::iterator nn;
    map< int , int > nodeCliqueRev;
    map<int, int> cliqueUsed;
    readSequencesFromFile(argv[1], nodeClique, cliqueUsed);

 //   cout<<" nodeClique size "<<nodeClique.size()<< " cliqueUsed "<<cliqueUsed.size()<<endl;


    for(nn = nodeClique.begin(); nn!=nodeClique.end(); nn++){
        nodeCliqueRev[nn->first] = nn->second.size();
	//cout<<" node "<<nn->first<<" number of cliques "<<nn->second.size()<<endl;
    }

    multimap<int, int > dst = flip_map(nodeCliqueRev);

    for(multimap<int, int>::const_reverse_iterator it = dst.rbegin(); it != dst.rend(); ++it)
        cout << "freq "<<it -> first << " node " << it -> second << endl; 



/*
    int pos = 0;
    for (const auto& seq : sequences) {
	vector< Shingles::Signature> multiSignatures;
	shingle.computeWSh(seq, multiSignatures, pos);
	clustersPos[multiSignatures].push_back(pos);
	clustersSizes[multiSignatures] = clustersPos[multiSignatures].size();
	pos++;
    }


    multimap<int, vector< Shingles::Signature> > dst = flip_map(clustersSizes);
    multimap<int, vector< Shingles::Signature > >::reverse_iterator cit;
    int c=0;
    map<int, set<int> >::iterator nit;
    vector<int> bitmapBc;
    vector<int> numberCs;
    vector<int> numCliquesInCluster;
    int seqXlen = 0;
    // Show clusters
    for(cit=dst.rbegin(); cit != dst.rend(); cit++){
	vector<int> allrow = clustersPos[cit->second];
	//if(allrow.size() < 2)continue;
	cout << "CLUSTER "<<c<<" size "<<allrow.size()<<" ";
	numCliquesInCluster.push_back(allrow.size());
	set<int> allnodescluster;
 	map<int, set<int> > nodeCliques;
	float sum = 0.0;
	//printVector(cit->second);
        for (unsigned int i=0; i<allrow.size(); i++){
	    sum += sequences[allrow[i]].size();
	    for(unsigned int j=0; j<sequences[allrow[i]].size(); j++){
	        set<int> setcliques;
                //cout << sequences[allrow[i]][j] << " ";
		allnodescluster.insert(sequences[allrow[i]][j]);
		nit = nodeCliques.find(sequences[allrow[i]][j]);
		if(nit != nodeCliques.end()){
			setcliques = nit->second;
		}
		setcliques.insert(i);
		nodeCliques[sequences[allrow[i]][j]] = setcliques;
	    }
            //cout << '\n';
        }
	cout<<"allnodescluster size "<<allnodescluster.size()<<" sum "<<sum<<" avg size included sets "<<sum/allrow.size()<<" cluster size "<<allrow.size()<<endl;
	numberCs.push_back(allnodescluster.size());
	appendSeqX(allnodescluster, seq, seqbin);
	appendBc(nodeCliques, bc, allrow.size(), allnodescluster);
        //cout << '\n';
	c++;
	seqXlen += allnodescluster.size();
    }
    seq.close();
    seqbin.close();
    bc.close();
    buildBitmapB(numberCs, outdir);
    int totalCliques = buildSeqNumCliques(numCliquesInCluster, outdir);
    cout<<"seqXlen "<<seqXlen<<" totalCliques "<<totalCliques<<" avg "<<(float)seqXlen/totalCliques<<endl;
*/
    return 0;
}
