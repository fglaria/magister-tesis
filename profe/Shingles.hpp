#ifndef SHINGLES_HPP_INCLUDED
#define SHINGLES_HPP_INCLUDED

#include <vector>
#include <cstddef>      // NULL, std::size_t
#include <string>
#include <cstdlib>      // std::rand, std::srand
#include <ctime>        // std::time

using namespace std;

class Shingles {
public:
    typedef unsigned int Signature;

    Shingles();
    Shingles(int nsh, int numhashes);

    template<typename ContainerT>
    void computeWSh(const ContainerT&, vector<Signature> &multi, int rowId);

private:
    static const unsigned long bigPrimeNumber = 0x7FFFFFFF; // Same as 2^31 - 1
    unsigned int A;     // A, as in a X + b mod bigPrimeNumber
    unsigned int B;     // B, as in a X + b mod bigPrimeNumber

    vector<unsigned int> A_;  // A_, as in a X + b mod prime_
    vector<unsigned int> B_;  // B_, as in a X + b mod prime_

    int numberSignatures;
    int nShingles;

};



inline
Shingles::Shingles() {
    srand(time(NULL));
    A = (rand() % bigPrimeNumber) + 1;
    B = (rand() % bigPrimeNumber) + 1;
    numberSignatures = 1;
    for (int i = 0; i < numberSignatures; i++){

      A_.push_back( rand() % bigPrimeNumber  + 1);
      B_.push_back( rand() % bigPrimeNumber + 1);
    }
    nShingles = 1;
}

Shingles::Shingles(int nsh, int nhashes){
    numberSignatures = nhashes;
    srand(time(NULL));

    for (int i = 0; i < numberSignatures; i++){

      A_.push_back( rand() % bigPrimeNumber  + 1);
      B_.push_back( rand() % bigPrimeNumber + 1);
    }
    nShingles = nsh;
}

template<typename ContainerT>
void
Shingles::computeWSh(const ContainerT& seqs, vector<Signature> & multiSignatures, int rowId) {

    hash<typename ContainerT::value_type> hashing;
    vector<size_t> vShingles(nShingles);
    int k = 0;
    size_t shingleID;
    for (int i = 0; i < numberSignatures; i++)
	multiSignatures.push_back(bigPrimeNumber);

    for (const auto& val : seqs) {
           size_t hashedWord = hashing(val);
	   vShingles[k%nShingles] = hashedWord;
	   if(k + 1 >= nShingles){
	      shingleID = 0;
	      for(unsigned int j=0; j < vShingles.size(); j++){
	         shingleID = shingleID + vShingles[j];
	      }
    	      for(int i=0; i<numberSignatures; i++){
                    Signature shingleHash = (((unsigned long) A_[i] * (unsigned long) shingleID) + B_[i]) % bigPrimeNumber;
                    if (shingleHash < multiSignatures[i]) {
                  	multiSignatures[i] = shingleHash;
              	    }
  	      }
           }
           k++;
    }
}


#endif  // SHINGLES_HPP_INCLUDED
