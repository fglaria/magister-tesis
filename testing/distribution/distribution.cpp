// g++ -std=c++11 distribution.cpp general.cpp -o distribution -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG

#include <vector>
#include <map>
#include <string>

#include <sdsl/int_vector.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/util.hpp>
#include <sdsl/rank_support.hpp>
#include <sdsl/select_support.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/suffix_arrays.hpp>

#include "general.hpp"

int get_distribution(sdsl::wm_int<sdsl::rrr_vector<63>> &, sdsl::rrr_vector<63> &,
    sdsl::wm_int<sdsl::rrr_vector<63>> &, unsigned char *, std::map<uint32_t, std::vector<uint32_t>> &);

int main(int argc, char* argv[]) {

    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    // Path to files
    const std::string path(argv[1]);

    // Iterations is 1, unless it's given
    const uint32_t iterations = argv[2] ? atoi(argv[2]) : 1;


    sdsl::wm_int<sdsl::rrr_vector<63>> x_wm;
    sdsl::rrr_vector<63> rrrB1;
    sdsl::wm_int<sdsl::rrr_vector<63>> y3_wm;
    unsigned char *B4 = general::readData(path, x_wm, rrrB1, y3_wm);

    std::map<uint32_t, std::vector<uint32_t>> distribution;

    for(int i = 1; i <= iterations; ++i)
    {
        get_distribution(x_wm, rrrB1, y3_wm, B4, distribution);
    }

    for(auto partition : distribution)
    {
        std::cout << partition.first << ":";

        for(auto data : partition.second)
        {
            std::cout << " " << data;
        }

        std::cout << std::endl;
    }


    return 0;
}


int get_distribution(
    sdsl::wm_int<sdsl::rrr_vector<63>> &x_wm,
    sdsl::rrr_vector<63> &rrrB1,
    sdsl::wm_int<sdsl::rrr_vector<63>> &y3_wm,
    unsigned char *B4,
    std::map<uint32_t, std::vector<uint32_t>> &distribution)
{
    // SDSL variables for B1
    sdsl::rrr_vector<63>::rank_1_type rrrB1_rank(&rrrB1);
    sdsl::rrr_vector<63>::select_1_type rrrB1_sel(&rrrB1);

    uint32_t currentY = 0;
    uint32_t nextY = y3_wm[0];
    uint32_t currentB1Index = 0;
    uint32_t nextB1Index = rrrB1_sel(1);

    // How many partitions for this graph
    const uint32_t howManyPartitions = rrrB1_rank(rrrB1.size());

    // std::cout << howManyPartitions << std::endl;

    // For all partitions, let's see distributions
    for (uint32_t partition = 1; partition < howManyPartitions; ++partition)
    {
        // Update B1 indexes, and count elements in partition
        currentB1Index = nextB1Index;
        nextB1Index = rrrB1_sel(partition + 1);
        const uint32_t elements = nextB1Index - currentB1Index;

        // Update Y values, and get bytes on partition and per element
        currentY = nextY;
        nextY = y3_wm[partition];
        const uint32_t bytesInB4 = nextY - currentY;
        uint32_t bytesPerMask = bytesInB4/elements;

        std::vector<uint32_t> data;
        data.push_back(elements);


        uint8_t cliques = 0;
        if(1 == bytesInB4)
        {
            data.push_back(1);
        }
        else
        {
            for (uint32_t xAddr = 0; xAddr < elements; ++xAddr) {

                uint8_t byte = B4[currentY + xAddr*bytesPerMask];

                for (int bit = 8; bit > 0; ++bit) {
                    uint8_t bitValue = byte >> bit-1;
                    if(bitValue)
                    {
                        if(bit > cliques)
                        {
                            cliques = bit;
                        }
                    }
                }
            }
        }

        data.push_back(cliques);

        distribution[partition] = data;
    }
}
