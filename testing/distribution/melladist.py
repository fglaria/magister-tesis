# -*- coding: utf-8 -*-
import sys
import matplotlib
import matplotlib.pyplot as plt
from collections import Counter

if 2 > len(sys.argv):
    print("Modo de uso: " + sys.argv[0] + " FILEPATH")
    sys.exit()

filepath = sys.argv[1]

listFile = open(filepath, 'r')

cliques = []

for line in listFile:
    line = line.split('_')
    line = line[0].split(' ')

line.pop()
line = list(map(int, line))


cliqFreq = Counter(line)
# print(cliqFreq)
xC = cliqFreq.keys()
heightC = cliqFreq.values()


fig, ax = plt.subplots()

p1 = ax.plot(xC, heightC, '.')

plt.title("Cantidad de particiones por cantidad de cliques")
# plt.xticks(x)
plt.xlabel("# cliques")
# plt.xlim([0, 750])
plt.xlim(xmax=750)
plt.xlim(xmin=-5)
plt.ylabel("# particiones")
# plt.ylim([-5, 250000])
plt.ylim(ymax=250000)
# plt.legend(('Jaccard', 'Sorensen'))
plt.grid(True)
plt.show()
plt.close()
