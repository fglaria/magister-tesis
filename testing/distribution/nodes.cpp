// g++ -std=c++11 nodes.cpp general.cpp -o nodes -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG

#include <vector>
#include <map>
#include <string>

#include <sdsl/int_vector.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/util.hpp>
#include <sdsl/rank_support.hpp>
#include <sdsl/select_support.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/suffix_arrays.hpp>

#include "general.hpp"

int get_distribution(sdsl::wm_int<sdsl::rrr_vector<63>> &, sdsl::rrr_vector<63> &,
    sdsl::wm_int<sdsl::rrr_vector<63>> &, std::map<uint32_t, uint32_t> &);

int main(int argc, char* argv[]) {

    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    // Path to files
    const std::string path(argv[1]);

    // Iterations is 1, unless it's given
    const uint32_t iterations = argv[2] ? atoi(argv[2]) : 1;


    sdsl::wm_int<sdsl::rrr_vector<63>> x_wm;
    sdsl::rrr_vector<63> rrrB1;
    sdsl::wm_int<sdsl::rrr_vector<63>> y_wm;
    general::readDataOld(path, x_wm, rrrB1, y_wm);

    std::map<uint32_t, uint32_t> distribution;

    for(int i = 1; i <= iterations; ++i)
    {
        get_distribution(x_wm, rrrB1, y_wm, distribution);
    }

    for(auto partition : distribution)
    {
        std::cout << partition.first << " " << partition.second << std::endl;
    }


    return 0;
}


int get_distribution(
    sdsl::wm_int<sdsl::rrr_vector<63>> &x_wm,
    sdsl::rrr_vector<63> &rrrB1,
    sdsl::wm_int<sdsl::rrr_vector<63>> &y_wm,
    std::map<uint32_t, uint32_t> &distribution)
{
    // SDSL variables for B1
    sdsl::rrr_vector<63>::rank_1_type rrrB1_rank(&rrrB1);
    sdsl::rrr_vector<63>::select_1_type rrrB1_sel(&rrrB1);

    // uint32_t currentY = 0;
    // uint32_t nextY = y_wm[0];
    uint32_t currentB1Index = 0;
    uint32_t nextB1Index = 0;

    // How many partitions for this graph
    const uint32_t howManyPartitions = rrrB1_rank(rrrB1.size());

    // std::cout << howManyPartitions << std::endl;

    // For all partitions, let's see distributions
    for (uint32_t partition = 1; partition < howManyPartitions-1; ++partition)
    {
        uint32_t cliques = y_wm[partition];

        nextB1Index = rrrB1_sel(partition + 1);

        // std::cout << "Partition " << partition << " Cliques " << cliques;
        // std::cout << " Current " << currentB1Index << " Next " << nextB1Index;
        // std::cout << " X ";

        for (uint32_t xAddress = currentB1Index; xAddress < nextB1Index; ++xAddress)
        {
            uint32_t xValue = x_wm[xAddress];
            // std::cout << xValue << " ";
            distribution[xValue] += cliques;
        }
        // std::cout << std::endl;

        currentB1Index = nextB1Index;
    }
}
