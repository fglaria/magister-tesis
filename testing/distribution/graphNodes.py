# -*- coding: utf-8 -*-
import sys
# from mpl_toolkits.mplot3d import Axes3D
import matplotlib
import matplotlib.pyplot as plt
from collections import Counter

if 2 > len(sys.argv):
    print("Modo de uso: " + sys.argv[0] + " FILEPATH")
    sys.exit()

filepath = sys.argv[1]

listFile = open(filepath, 'r')

nodes = []
cliques = []

print "Lines"
for line in listFile:
    line = line.split(' ')

    nodes.append(int(line[0]))
    cliques.append(int(line[1]))

print "Graph"
fig, ax = plt.subplots()

p1 = ax.bar(nodes, cliques)

plt.title("Cantidad de cliques para cada nodo")
# plt.xticks(x)
plt.xlabel("# nodo")
# plt.xlim([0, 260])
# plt.xlim(xmax=750)
# plt.xlim(xmin=-5)
plt.ylabel("# cliques")
# plt.ylim([-5, 240000])
# plt.ylim(ymax=250000)
# plt.legend(('Jaccard', 'Sorensen'))
plt.grid(True)
plt.show()
plt.close()
