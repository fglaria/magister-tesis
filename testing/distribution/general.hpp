#ifndef GENERAL_LIBRARY
#define GENERAL_LIBRARY

#include <iostream>
#include <fstream>
#include <set>
#include <map>

#include <sdsl/int_vector.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/util.hpp>
#include <sdsl/rank_support.hpp>
#include <sdsl/select_support.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/suffix_arrays.hpp>

namespace general
{
    unsigned char *readData(
        const std::string path,
        sdsl::wm_int<sdsl::rrr_vector<63>> &x_wt,
        sdsl::rrr_vector<63> &rrrB1,
        sdsl::wm_int<sdsl::rrr_vector<63>> &y3_wt
    );

    void readDataB4(
        const std::string path,
        sdsl::wm_int<sdsl::rrr_vector<63>> &x_wt,
        sdsl::rrr_vector<63> &rrrB1,
        sdsl::wt_hutu<sdsl::rrr_vector<63>> &b4_wt,
        sdsl::wm_int<sdsl::rrr_vector<63>> &y3_wt
    );

    void readDataOld(
        const std::string path,
        sdsl::wm_int<sdsl::rrr_vector<63>> &x_wt,
        sdsl::rrr_vector<63> &rrrB1,
        sdsl::wm_int<sdsl::rrr_vector<63>> &y_wt
    );
}

#endif