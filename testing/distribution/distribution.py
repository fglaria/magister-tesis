# -*- coding: utf-8 -*-
import sys
# from mpl_toolkits.mplot3d import Axes3D
import matplotlib
import matplotlib.pyplot as plt
from collections import Counter

if 2 > len(sys.argv):
    print("Modo de uso: " + sys.argv[0] + " FILEPATH")
    sys.exit()

filepath = sys.argv[1]

listFile = open(filepath, 'r')

partitions = []
nodes = []
cliques = []

for line in listFile:
    line = line.split(':')

    partitions.append(int(line[0]))
    values = line[1].split(' ')

    nodes.append(int(values[1]))
    cliques.append(int(values[2]))

cliqFreq = Counter(cliques)
# print(cliqFreq)
xC = cliqFreq.keys()
heightC = cliqFreq.values()

nodesFreq = Counter(nodes)
# print(nodesFreq)
xN = nodesFreq.keys()
heightN = nodesFreq.values()


width = 1

color = '#2c7fb8'

fig, ax = plt.subplots()

p1 = ax.plot(xC, heightC, '.')

plt.title("Cantidad de particiones por cantidad de cliques")
# plt.xticks(x)
plt.xlabel("# cliques")
# plt.xlim([0, 260])
plt.xlim(xmax=750)
plt.xlim(xmin=-5)
plt.ylabel("# particiones")
# plt.ylim([-5, 240000])
plt.ylim(ymax=250000)
# plt.legend(('Jaccard', 'Sorensen'))
plt.grid(True)
plt.show()
plt.close()


p2 = ax.plot(xN, heightN, '.')

plt.title("Cantidad de particiones por cantidad de nodos")
# plt.xticks(x)
plt.xlabel("# nodos")
# plt.xlim([0, 200])
plt.xlim(xmax=200)
plt.xlim(xmin=-5)
plt.ylabel("# particiones")
# plt.ylim([-5, 100000])
plt.ylim(ymax=100000)
# plt.legend(('Jaccard', 'Sorensen'))
plt.grid(True)
plt.show()

