#include <string>
#include <sstream>
// #include <vector>
// #include <iostream>

#include <sdsl/int_vector.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/util.hpp>
#include <sdsl/rank_support.hpp>
#include <sdsl/select_support.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/suffix_arrays.hpp>

// template <class type> void writeFileandBinary(std::vector<type> numbers, std::ofstream &seq, std::ofstream &seqbin);

uint32_t compressInt(std::string filename);
uint32_t compressByte(std::string filename);
uint32_t compressBitmap(std::string filename);
