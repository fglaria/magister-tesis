#include "common.hpp"


// template <class type> void writeFileandBinary(std::vector<type> numbers, std::ofstream &seq, std::ofstream &seqbin){
//     type *buffer = new type[numbers.size()];
//     uint32_t end = numbers.end() - numbers.begin();

//     for(uint32_t i=0; i<end; ++i)
//     {
//         seq << (uint32_t) numbers[i] << " ";
//         buffer[i] = numbers[i];
//     }

//     seqbin.write((char *)&*buffer, (numbers.size())*sizeof(type));
//     delete[] buffer;
// }

uint32_t compressInt(std::string filename) {
    // sdsl::wt_int<sdsl::rrr_vector<63>> wt_int;
    // sdsl::construct(wt_int, filename.c_str(), 4);
    // store_to_file(wt_int, filename + "-wt_int.sdsl");

    sdsl::wm_int<sdsl::rrr_vector<63>> wm_int;
    sdsl::construct(wm_int, filename.c_str(), 4);
    store_to_file(wm_int, filename + "-wm_int.sdsl");

    const uint32_t bitSize = size_in_bytes(wm_int)*8;
    std::cout << filename << " size " << bitSize << " bits" << std::endl;
    return bitSize;
}

uint32_t compressByte(std::string filename) {
    // sdsl::wt_int<sdsl::rrr_vector<63>> wt_int;
    // sdsl::construct(wt_int, filename.c_str(), 1);
    // store_to_file(wt_int, filename + "-wt_int.sdsl");

    // sdsl::wt_blcd<sdsl::rrr_vector<63>> wt_blcd;
    // sdsl::construct(wt_blcd, filename.c_str(), 1);
    // store_to_file(wt_blcd, filename + "-wt_blcd.sdsl");

    // sdsl::wt_huff<sdsl::rrr_vector<63>> wt_huff;
    // sdsl::construct(wt_huff, filename.c_str(), 1);
    // store_to_file(wt_huff, filename + "-wt_huff.sdsl");

    sdsl::wt_hutu<sdsl::rrr_vector<63>> wt_hutu;
    sdsl::construct(wt_hutu, filename.c_str(), 1);
    store_to_file(wt_hutu, filename + "-wt_hutu.sdsl");

    const uint32_t bitSize = size_in_bytes(wt_hutu)*8;
    std::cout << filename << " size " << bitSize << " bits" << std::endl;
    return bitSize;
}

uint32_t compressBitmap(std::string filename) {

    std::ifstream Bfile (filename.c_str());

    std::string sB1;
    std::vector<uint32_t> B1;
    // B1
    if (getline (Bfile, sB1))
    {
        Bfile.close();
    }
    else
    {
        std::cout << "Unable to open file Bfile" << std::endl;
        return -1;
    }
    std::stringstream ssB1(sB1);

    uint32_t number;
    while (ssB1 >> number)
    {
        B1.push_back(number);
    }

    size_t Blen = B1.size();

    sdsl::bit_vector B = sdsl::bit_vector(Blen, 0);
    for (size_t i=0; i <= Blen; i++) {
        if(B1[i] == 1)
        {
            B[i] = 1;
        }
    }

    sdsl::rrr_vector<63> rrrb(B);
    store_to_file(rrrb, filename + "-rrr-64.sdsl");

    const uint32_t bitSize = size_in_bytes(rrrb)*8;
    std::cout << filename << " size " << bitSize << " bits" << std::endl;

    sdsl::sd_vector<> sdb(B);
    store_to_file(sdb, filename + "-sdb.sdsl");

    return 0;
}