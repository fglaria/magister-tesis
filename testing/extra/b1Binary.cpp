// g++ -std=c++11 b1Binary.cpp -o b1Binary -O3 -DNDEBUG

#include <iostream>
#include <vector>
#include <unordered_set>
#include <map>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>


template <class type> void writeFileandBinary(std::vector<type> numbers, std::ofstream &seqbin){
    type *buffer = new type[numbers.size()];
    uint32_t end = numbers.end() - numbers.begin();

    for(uint32_t i=0; i<end; ++i)
    {
        buffer[i] = numbers[i];
    }

    seqbin.write((char *)&*buffer, (numbers.size())*sizeof(type));
    delete[] buffer;
}


int main(int argc, char* argv[]) {

    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    // Path to files
    const std::string basePath(argv[1]);

    std::string sB1 = basePath + ".B";

    std::ifstream Bfile (sB1.c_str());


    // B1
    if (getline (Bfile, sB1))
    {
        Bfile.close();
    }
    else
    {
        std::cerr << "Unable to open file Bfile" << std::endl;
        return -1;
    }


    // String to Vector of uint32_t
    std::stringstream ssB1(sB1);
    std::vector<uint32_t> B1;

    uint32_t number;

    while (ssB1 >> number)
    {
        B1.push_back(number);
    }
    // Removes last 1
    B1.pop_back();


    // File output
    std::ofstream b1FileBin(basePath + ".B1.bin", std::ofstream::out | std::ofstream::binary | std::ofstream::trunc);

    writeFileandBinary(B1, b1FileBin);

    return 0;
}
