// g++ -std=c++11 compress.cpp -o compress -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG

#include <string>
#include <fstream>
#include <sstream>
#include <vector>

#include <sdsl/int_vector.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/util.hpp>
#include <sdsl/rank_support.hpp>
#include <sdsl/select_support.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/suffix_arrays.hpp>

#include "../common/common.hpp"


int main(int argc, char const *argv[])
{
    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    std::cout << "Compressing..." << std::endl;
    // Path to files
    const std::string path(argv[1]);

    std::string sXbin = path+".X.bin";
    std::string sB1 = path+".B1";
    // std::string sB1 = path + ".B";
    std::string sB2bin = path+".B2.bin";
    std::string sYbin = path+".Y.bin";

    uint32_t totalSize = 0;

    totalSize += compressInt(sXbin);
    // std::cout << "X Done" << std::endl;

    totalSize += compressInt(sYbin);
    // std::cout << "Y Done" << std::endl;

    totalSize += compressBitmap(sB1);
    // std::cout << "B1 Done" << std::endl;

    totalSize += compressByte(sB2bin);
    // std::cout << "B2 Done" << std::endl;

    std::cout << "total size " << totalSize << " bits" << std::endl;


    if(3 == argc)
    {
        std::cout << "BPE " << totalSize/atof(argv[2]) << std::endl;
    }

    return 0;
}