import sys
import os

if 2 > len(sys.argv):
    print "Modo de uso: " + sys.argv[0] + " RUTA"
    sys.exit()

# Path to file
path = sys.argv[1]

extensions = (
    "X.bin",
    "B1.bin",
    "B2.bin",
    "Y.bin"
)

total = 0

for extension in extensions:
    statinfo = os.stat(path + '.' + extension)
    size = statinfo.st_size*8

    print extension + " size " + str(size) + " bits"

    total += size

print "total " + str(total) + " bits"


if 3 == len(sys.argv):
    print "BPE " + str(total/float(sys.argv[2]))