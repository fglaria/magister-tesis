// g++ -std=c++11 popCliques.cpp -o popCliques -O3 -DNDEBUG

#include <iostream>
#include <unordered_set>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
// #include <bitset>
// #include <unordered_set>
#include <functional>

#include <algorithm>

int main(int argc, char* argv[]) {

    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    // Path to files
    const std::string basePath(argv[1]);

    const std::string cliqueFile = basePath + ".clique.txt";

    std::ifstream fileStream;
    std::string lineFile;

    std::vector<std::set<uint32_t>> cliques;
    std::map<uint32_t, uint32_t> countNodes;
    std::map<uint32_t, std::set<uint32_t>> nodeCliques;
    uint32_t howManyCliques = 0;


    // Open cliques file & create cliques vector
    fileStream.open(cliqueFile.c_str(), std::ios::in);

    if(fileStream.is_open())
    {
        while(getline(fileStream, lineFile))
        {
            std::stringstream lineStream(lineFile);
            uint32_t node;
            std::set<uint32_t> clique;

            while(lineStream >> node)
            {
                clique.insert(node);
                countNodes[node] += 1;

                // Check if clique exist to insert node, create it and insert node if not
                std::map<uint32_t, std::set<uint32_t>>::iterator iter = nodeCliques.find(node);
                if(nodeCliques.end() == iter)
                {
                    std::set<uint32_t> us;
                    us.insert(howManyCliques);
                    nodeCliques[node] = us;
                }
                else
                {
                    iter->second.insert(howManyCliques);
                }
            }

            cliques.push_back(clique);

            ++howManyCliques;
        }
    }
    else
    {
        std::cerr << "No clique file" << std::endl;
        return -1;
    }

    std::cerr << "cliques: " << cliques.size() << std::endl;
    std::cerr << "countNodes: " << countNodes.size() << std::endl;
    std::cerr << "nodeCliques: " << nodeCliques.size() << std::endl;
    std::cerr << "howManyCliques: " << howManyCliques << std::endl;

    // for(const auto &cSet: cliques)
    // {
    //     for(const auto &n: cSet)
    //     {
    //         std::cout << n << " ";
    //     }
    //     std::cout << std::endl;
    // }
    // for(const auto &pair : countNodes)
    // {
    //     std::cout << pair.first << " " << pair.second << std::endl;
    // }


    // Order nodes by popularity
    typedef std::function<bool(std::pair<uint32_t, uint32_t>, std::pair<uint32_t, uint32_t>)> Comparator;

    Comparator compFunctor = [](std::pair<uint32_t, uint32_t> elem1 ,std::pair<uint32_t, uint32_t> elem2)
    {
        if (elem1.second == elem2.second)
        {
            return elem1.first < elem2.first;
        }

        return elem1.second >= elem2.second;
    };

    std::set<std::pair<uint32_t, uint32_t>, Comparator> popNodes(countNodes.begin(), countNodes.end(), compFunctor);
    countNodes.clear();


    std::cerr << "popNodes: " << popNodes.size() << std::endl;

    // for(const auto &pair: popNodes)
    // {
    //     std::cout << pair.first << " " << pair.second;
    //     std::cout << std::endl;
    // }


    // Bool vector to check if a clique is not repeated
    std::vector<bool> uniqueCliques(howManyCliques, 0);
    uint32_t uniqueCount = 0;
    uint32_t pushbacks = 0;

    // For every popular node, from most to less popular
    for(const auto &pair : popNodes)
    {
        // std::cerr << "FOR1" << std::endl;
        const uint32_t &popNode = pair.first;
        // std::cerr << popNode << ": ";

        // Get set of cliques of popular node
        const std::set<uint32_t> &popCliques = nodeCliques[popNode];

        uint8_t unique = false;
        for(const auto &cliqueIndex : popCliques)
        {
            // std::cerr << "cliqueIndex: " << cliqueIndex;
            if(0 == uniqueCliques[cliqueIndex])
            {
                uniqueCliques[cliqueIndex] = 1;
                std::cout << cliqueIndex << " ";
                // std::cerr << cliqueIndex << " ";
                unique = true;
                ++uniqueCount;
                // std::cerr << " unique";
            }
            // std::cerr << std::endl;
        }

        if(unique)
        {
            std::cout << std::endl;
            ++pushbacks;
            // std::cerr << std::endl;
        }
    }

    for (uint32_t i = 0; i < uniqueCliques.size(); ++i) {
        if(0 == uniqueCliques[i])
        {
            std::cout << i << std::endl;
            // std::cerr << i << std::endl;
            ++uniqueCount;
            ++pushbacks;
        }
        // else {
        //     std::cerr << "ELSE" << std::endl;
        // }
    }

    uint32_t zeros = std::count(uniqueCliques.begin(), uniqueCliques.end(), 0);

    // std::cerr << "uniqueCliques: " << uniqueCliques.size() << std::endl;
    std::cerr << "uniqueCount: " << uniqueCount << std::endl;
    std::cerr << "pushbacks: " << pushbacks << std::endl;
    // std::cerr << "zeros: " << zeros << std::endl;



    return 0;
}
