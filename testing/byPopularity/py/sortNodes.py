# -*- coding: utf-8 -*-
import sys
# from mpl_toolkits.mplot3d import Axes3D
import matplotlib
import matplotlib.pyplot as plt
from collections import Counter

if 2 > len(sys.argv):
    print("Modo de uso: " + sys.argv[0] + " FILEPATH")
    sys.exit()

filepath = sys.argv[1]

listFile = open(filepath, 'r')

popularity = {}
max = 0

for line in listFile:
    line = line.split(' ')

    for node in line:
        node = int(node)
        if node in popularity:
            popularity[node] += 1
        else:
            popularity[node] = 1

        if node > max:
            max = node

# print(popularity)

sort = [(k, popularity[k]) for k in sorted(popularity, key=popularity.get, reverse=True)]

for node in sort:
    # print(node)
    print(str(node[0]) + " " + str(node[1]))
