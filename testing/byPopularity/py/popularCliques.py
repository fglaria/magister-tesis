# -*- coding: utf-8 -*-
import sys

from collections import Counter

if 2 > len(sys.argv):
    print("Modo de uso: " + sys.argv[0] + " FILEPATH")
    sys.exit()

filepath = sys.argv[1]

cliquesFile = open(filepath, 'r')

popNodes = {}
nodesOnCliques = {}


for cIndex, line in enumerate(cliquesFile):
    line = line.split(' ')

    for node in line:
        node = int(node)
        if node in popNodes:
            popNodes[node] += 1
        else:
            popNodes[node] = 1

        if node not in nodesOnCliques:
            nodesOnCliques[node] = []
        nodesOnCliques[node].append(cIndex)


sort = [(k, popNodes[k]) for k in sorted(popNodes, key=popNodes.get, reverse=True)]

outLine = ""
for n in sort:
    print(' '.join(str(c) for c in nodesOnCliques[n[0]]))