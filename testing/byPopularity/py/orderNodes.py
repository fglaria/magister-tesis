# -*- coding: utf-8 -*-
import sys
# from mpl_toolkits.mplot3d import Axes3D
import matplotlib
import matplotlib.pyplot as plt
from collections import Counter

if 2 > len(sys.argv):
    print("Modo de uso: " + sys.argv[0] + " FILEPATH")
    sys.exit()

filepath = sys.argv[1]

listFile = open(filepath, 'r')

nodes = {}
max = 0

for line in listFile:
    line = line.split(' ')

    for node in line:
        node = int(node)
        if node in nodes:
            nodes[node] += 1
        else:
            nodes[node] = 1

        if node > max:
            max = node

sort = [(k, nodes[k]) for k in sorted(nodes, key=nodes.get, reverse=True)]

outLine = ""
for n in sort:
    outLine += str(n[0]) + " "

print(outLine)