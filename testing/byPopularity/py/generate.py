# -*- coding: utf-8 -*-
import sys

# from collections import OrderedSet

cliquesFilepath = "../cliques/dblp-2011-min.all-cliques.txt"
popularityFilepath = "sorted.txt"
nOCFilepath = "nodesOnCliques.txt"
# cliquesFilepath = "../cliques/mini.txt"
# popularityFilepath = "sorted.mini.txt"
# nOCFilepath = "nodesOnCliques.mini.txt"


cliquesFile = open(cliquesFilepath, 'r')
cliques = []

print("Reading cliques")
for cIndex, line in enumerate(cliquesFile):
    line = line.split(' ')

    clique = []
    for node in line:
        node = int(node)
        clique.append(node)

    cliques.append(clique)

cliquesFile.close()


print("Reading nodes on cliques")
nOCFile = open(nOCFilepath, 'r')
nodesOnCliques = {}

for line in nOCFile:
    line = line.split(':')
    n = int(line[0])
    nodesOnCliques[n] = []

    line = line[1].split(' ')
    for c in line:
        c = int(c)
        nodesOnCliques[n].append(c)

nOCFile.close()


print("Reading popularity")
popularityFile = open(popularityFilepath, 'r')
popularity = []

for line in popularityFile:
    line = line.split(' ')
    popularity.append(int(line[0]))

popularityFile.close()

print("Generating X B1 B2 Y")
Xlist = []
B1list = []
B2list = []
Ylist = [0]
myY = Ylist[0]

uniqueXList = []

# For every ranked popular node, from most to less
for popNode in popularity:
    popXList = []

    popCliques = nodesOnCliques[popNode]

    howManyCliques = 0

    # How many bytes in B2 has every node
    bytesPerX = (len(popCliques) - 1)//8 + 1

    # For every clique with popular node
    for c in popCliques:
        # Temporal node set of nodes on clique
        tempXList = []
        # Should merge Temporal node set to pop
        merge = False

        # For every node on current clique
        for n in cliques[c]:
            tempXList.append(n)

            # If node isn't in another partition
            if n not in uniqueXList:
                merge = True

                uniqueXList.append(n)
                if 0 == len(popXList):
                    popXList.extend(tempXList)
                    # for temp in tempXList:
                    #     B2list.append(0)
                else:
                    popXList.append(n)
                    B2list.append(0)
        # print(B2list)


        # If temporal node Set has node that wasn't on another partition, merge
        if merge:
            # # B2 magic !!!!
            # for popIndex, popN in enumerate(popXList):

            #     B2list[myY+popIndex] *= 2

            #     if popN in tempXList:
            #         B2list[myY+popIndex] += 1

            howManyCliques += 1

    # print(B2list)


    # If not empty
    if 0 != len(popXList):
        Xlist.append(popXList)

        # How many bytes in B2 are in total on this partition
        myY += bytesPerX * len(popXList) if howManyCliques != 1 else 1
        Ylist.append(myY)


print(len(Xlist))
print(len(B2list))

print("Writting files")

outFilePath = "dblp-2011"
# outFilePath = "dblp-2011.mini"

X1file = open(outFilePath+".X", 'w')

for xlist in Xlist:
    for index, x in enumerate(xlist):
        if 0 == index:
            B1list.append(1)
        else:
            B1list.append(0)

        X1file.write("%s " % x)
X1file.close()

B1list.append(1)

B1file = open(outFilePath+".B1", 'w')
for b in B1list:
    B1file.write("%s " % b)
B1file.close()

B2file = open(outFilePath+".B2", 'w')
for b in B2list:
    B2file.write("%s " % b)
B2file.close()

Yfile = open(outFilePath+".Y", 'w')
for y in Ylist:
    Yfile.write("%s " % y)
Yfile.close()
