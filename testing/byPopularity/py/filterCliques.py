# -*- coding: utf-8 -*-
import sys

from collections import Counter

if 2 > len(sys.argv):
    print("Modo de uso: " + sys.argv[0] + " FILEPATH")
    sys.exit()

filepath = sys.argv[1]

cliquesFile = open(filepath, 'r')

uniqueCliques = []
count = 0

for line in cliquesFile:
    line = line.split(' ')

    newLine = ""

    for node in line:
        node = int(node)

        if node in uniqueCliques:
            continue

        uniqueCliques.append(node)
        newLine += str(node) + " "

    if "" != newLine:
        print(newLine)

    # print(count)
    # count += 1
