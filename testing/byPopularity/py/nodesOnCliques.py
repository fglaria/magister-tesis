# -*- coding: utf-8 -*-
import sys

if 2 > len(sys.argv):
    print("Modo de uso: " + sys.argv[0] + " FILEPATH")
    sys.exit()

cliquesFilepath = sys.argv[1]

cliquesFile = open(cliquesFilepath, 'r')
cliques = []
nodesOnCliques = {}

for cIndex, line in enumerate(cliquesFile):
    line = line.split(' ')

    clique = []
    for node in line:
        node = int(node)
        # clique.append(node)

        if node not in nodesOnCliques:
            nodesOnCliques[node] = []
        nodesOnCliques[node].append(cIndex)

    # cliques.append(clique)

cliquesFile.close()

for node in nodesOnCliques.keys():
    print(str(node) + ' ' + ' '.join(str(c) for c in nodesOnCliques[node]))