// g++ -std=c++11 popular.cpp binary_functions.cpp -o popular -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64 -O3 -DNDEBUG

#include <vector>
#include <unordered_set>
#include <map>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <sdsl/int_vector.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/util.hpp>
#include <sdsl/rank_support.hpp>
#include <sdsl/select_support.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/suffix_arrays.hpp>

#include "binary_functions.h"



template <class type> void writeFileandBinary(std::vector<type> numbers, std::ofstream &seq, std::ofstream &seqbin){
    type *buffer = new type[numbers.size()];
    uint32_t end = numbers.end() - numbers.begin();

    for(uint32_t i=0; i<end; ++i)
    {
        seq << (uint32_t) numbers[i] << " ";
        buffer[i] = numbers[i];
    }

    seqbin.write((char *)&*buffer, (numbers.size())*sizeof(type));
    delete[] buffer;
}


int main(int argc, char* argv[]) {

    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    // Path to files
    const std::string basePath(argv[1]);

    const std::string popFile = basePath + ".upop.txt";
    const std::string cliqueFile = basePath + ".clique.txt";

    std::ifstream fileStream;
    std::string lineFile;

    // std::cout << "Read upop" << std::endl;

    // Read upop file
    fileStream.open(popFile.c_str(), std::ios::in);

    std::vector<std::vector<uint32_t>> popularCliques;

    if (fileStream.is_open())
    {
        while (getline(fileStream, lineFile))
        {
            std::stringstream lineStream(lineFile);
            uint32_t number;
            std::vector<uint32_t> v;

            while(lineStream >> number)
            {
                v.push_back(number);
            }
            popularCliques.push_back(v);
        }

        fileStream.close();
    }
    else
    {
        std::cerr << "No upop file" << std::endl;
        return -1;
    }

    std::cerr << "popularCliques size: " << popularCliques.size() << std::endl;

    // for(auto v : popularCliques)
    // {
    //     for(auto n : v)
    //     {
    //         std::cout << n << " ";
    //     }
    //     std::cout << std::endl;
    // }



    // std::cout << "Read clique" << std::endl;

    // Read clique file
    fileStream.open(cliqueFile.c_str(), std::ios::in);

    std::vector<std::vector<uint32_t>> cliques;

    if (fileStream.is_open())
    {
        while (getline(fileStream, lineFile))
        {
            std::stringstream lineStream(lineFile);
            uint32_t number;
            std::vector<uint32_t> v;

            while(lineStream >> number)
            {
                v.push_back(number);
            }

            cliques.push_back(v);
        }

        fileStream.close();
    }
    else
    {
        std::cerr << "No clique file" << std::endl;
        return -1;
    }

    std::cerr << "cliques size: " << cliques.size() << std::endl;


    std::vector<std::set<uint32_t>> X;
    std::vector<uint8_t> B1;
    std::vector<std::vector<uint8_t>> B2;
    std::vector<uint32_t> Y;
    Y.push_back(0);


    // popularCliques, cliques
    // std::cout << "Generating partitions" << std::endl;


    // For every popular list of cliques
    for(const auto &popCliques : popularCliques)
    {
        const uint32_t howManyCliques = popCliques.size();
        // std::cout << howManyCliques << " ----" << std::endl;

        std::set<uint32_t> partitionNodes;

        for(const auto &cliqueIndex : popCliques)
        {
            // std::cout << partitionNodes.size() << " --" << std::endl;
            for(const auto &node : cliques[cliqueIndex])
            {
                partitionNodes.insert(node);
            }
        }
        // std::cout << "Done partition " << partitionNodes.size() << std::endl;



        const uint32_t bytesPerX = ((howManyCliques - 1) / 8) + 1;

        // const uint32_t howManyBytes = howManyCliques != 1 ? bytesPerX * partitionNodes.size() : 1;
        const uint32_t howManyBytes = howManyCliques != 1 ? bytesPerX * partitionNodes.size() : 0;
        // const uint32_t howManyBytes = bytesPerX * partitionNodes.size();

        // std::cerr << "howManyCliques: " << howManyCliques << std::endl;
        // std::cerr << "bytesPerX: " << bytesPerX << std::endl;
        // std::cerr << "partitionNodes: " << partitionNodes.size() << std::endl;
        // std::cerr << "howManyBytes: " << howManyBytes << std::endl;

        X.push_back(partitionNodes);
        B1.push_back('1');
        B1.insert(B1.end(), partitionNodes.size() - 1, '0');
        Y.push_back(howManyBytes + Y.back());

        std::vector<uint8_t> b2Partition(howManyBytes, 0);

        if(0 != howManyBytes)
        {
            // std::cout << "B2" << std::endl;
            // std::vector<uint8_t> b2Partition(howManyBytes, 0);

            for(uint32_t popCliqueIndex = 0; popCliqueIndex < popCliques.size(); ++popCliqueIndex)
            {
                const std::vector<uint32_t> &nClique = cliques[popCliques[popCliqueIndex]];

                for(const auto &node : nClique)
                {
                    std::set<uint32_t>::const_iterator partitionIter = partitionNodes.find(node);
                    if(partitionNodes.cend() != partitionIter)
                    {
                        // Get index of node on partitionNode based on distance between its begining and iterator
                        const uint32_t nodeIndex = std::distance(partitionNodes.cbegin(), partitionIter);

                        const uint32_t b2Index = (bytesPerX * nodeIndex) + (popCliqueIndex / 8);
                        const uint8_t bitIndex = popCliqueIndex % 8;

                        b2Partition[b2Index] |= 1 << bitIndex;
                    }
                }
            }

            B2.push_back(b2Partition);
        }

        // for(const auto &node : partitionNodes)
        // {
        //     std::cerr << node << " ";
        // }
        // std::cerr << std::endl;

        // for(const auto &byte : b2Partition)
        // {
        //     std::cerr << binary::print(byte, 8, false) << " ";
        // }
        // std::cerr << std::endl;

        // uint32_t i = 0;
        // for (const auto &node : partitionNodes) {
        //     std::cerr << node << ": ";

        //     for (int j = 0; j < bytesPerX; ++j) {
        //         std::cerr << binary::print(b2Partition[i*bytesPerX + j], 8, false) << " ";
        //     }

        //     std::cerr << std::endl;
        //     ++i;
        // }

        // return 0;
    }

    B1.push_back('1');

    std::cerr << "X size " << X.size() << std::endl;
    std::cerr << "B1 size " << B1.size() << std::endl;
    std::cerr << "B2 size " << B2.size() << std::endl;
    std::cerr << "Y size " << Y.size() << std::endl;


    // File output
    std::ofstream xFile(basePath + ".X", std::ofstream::out | std::ofstream::trunc);
    std::ofstream b1File(basePath + ".B1", std::ofstream::out | std::ofstream::trunc);
    std::ofstream b2File(basePath + ".B2", std::ofstream::out | std::ofstream::trunc);
    std::ofstream yFile(basePath + ".Y", std::ofstream::out | std::ofstream::trunc);

    std::ofstream xFileBin(basePath + ".X.bin", std::ofstream::out | std::ofstream::binary | std::ofstream::trunc);
    std::ofstream b1FileBin(basePath + ".B1.bin", std::ofstream::out | std::ofstream::binary | std::ofstream::trunc);
    std::ofstream b2FileBin(basePath + ".B2.bin", std::ofstream::out | std::ofstream::binary | std::ofstream::trunc);
    std::ofstream yFileBin(basePath + ".Y.bin", std::ofstream::out | std::ofstream::binary | std::ofstream::trunc);

    std::vector<uint32_t> vX;
    std::vector<uint8_t> vB2;

    for(auto x : X)
    {
        for(auto n : x)
        {
            vX.push_back(n);
        }
    }

    for(auto b2 : B2)
    {
        for(auto b : b2)
        {
            vB2.push_back(b);
        }
    }


    writeFileandBinary(vX, xFile, xFileBin);
    // writeFileandBinary(B1, b1File, b1FileBin);
    writeFileandBinary(vB2, b2File, b2FileBin);
    writeFileandBinary(Y, yFile, yFileBin);

    // for(auto x : X)
    // {
    //     for(auto n : x)
    //     {
    //         // std::cout << n << " ";
    //         xFile << n << " ";
    //     }
    //     // std::cout << std::endl;
    // }
    // xFile.close();

    for(auto b1 : B1)
    {
        b1File << b1 << " ";
    }
    b1File.close();

    // for(auto b2 : B2)
    // {
    //     for(auto byte : b2)
    //     {
    //         b2File << unsigned(byte)  << " ";
    //     }
    // }
    // b2File.close();

    // for(auto y : Y)
    // {
    //     yFile << y << " ";
    // }
    // yFile.close();


    return 0;
}
