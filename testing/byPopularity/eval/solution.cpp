#include "solution.h"

namespace solution
{
    // read compressed data and returns pointer to B4
    unsigned char * readData(const std::string path, sdsl::wm_int<sdsl::rrr_vector<63>> &x_wt,
        sdsl::rrr_vector<63> &rrrB1, sdsl::wm_int<sdsl::rrr_vector<63>> &y3_wt)
    {
        // Paths to SDSL & binary files
        std::string sXbin = path+".X.bin-wm_int.sdsl";
        std::string sB1bin = path+".B1-rrr-64.sdsl";
        std::string sB4bin = path+".B2.bin";
        std::string sY3bin = path+".Y.bin-wm_int.sdsl";

        // Open B4 file
        std::ifstream b4file (sB4bin, std::ios::in | std::ios::binary | std::ios::ate);

        // Reading files
        // Load X, B1
        load_from_file(x_wt, sXbin.c_str());
        load_from_file(rrrB1, sB1bin.c_str());
        // Construct Y3
        load_from_file(y3_wt, sY3bin.c_str());

        // Read B4
        std::streampos size = b4file.tellg();
        unsigned char * B4 = new unsigned char [size];
        b4file.seekg (0, std::ios::beg);
        b4file.read ((char*)B4, size);
        b4file.close();

        return B4;
    }


    // read compressed data
    void readDataB4(const std::string path, sdsl::wm_int<sdsl::rrr_vector<63>> &x_wm,
        sdsl::rrr_vector<63> &rrrB1, sdsl::wt_hutu<sdsl::rrr_vector<63>> &b4_wt,
        sdsl::wm_int<sdsl::rrr_vector<63>> &y3_wm)
    {
        // Paths to SDSL & binary files
        std::string sXbin = path+".X.bin-wm_int.sdsl";
        std::string sB1bin = path+".B1-rrr-64.sdsl";
        std::string sB4bin = path+".B2.bin-wt_hutu.sdsl";
        std::string sY3bin = path+".Y.bin-wm_int.sdsl";

        // // Open B4 file
        // std::ifstream b4file (sB4bin, std::ios::in | std::ios::binary | std::ios::ate);

        // Reading files
        // Load X, B1, B4, Y3
        load_from_file(x_wm, sXbin.c_str());
        load_from_file(rrrB1, sB1bin.c_str());

        load_from_file(b4_wt, sB4bin.c_str());
        load_from_file(y3_wm, sY3bin.c_str());
    }



    std::string time_representation(double t) {
        std::string units = " [ms]";

        if (1000 < t)
        {
            t /= 1000;
            units = " [s]";
        }

        return std::to_string(t) + units;
    }

}
