// g++ -std=c++11 format.cpp -o format

#include <stdio.h>
#include <stdlib.h>

#include <cstdlib>
#include <map>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <iostream>

template <class type> void writeFileandBinary(std::vector<type> numbers, std::ofstream &seq, std::ofstream &seqbin){
    type *buffer = new type[numbers.size()];
    uint32_t end = numbers.end() - numbers.begin();

    for(uint32_t i=0; i<end; ++i)
    {
        seq << (uint32_t) numbers[i] << " ";
        buffer[i] = numbers[i];
    }

    seqbin.write((char *)&*buffer, (numbers.size())*sizeof(type));
    delete[] buffer;
}

int main(int argc, char* argv[]) {

    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    // Path to files
    const std::string path(argv[1]);

    std::string sY = path + ".seqY3";

    std::ifstream seqBCfile (sY.c_str());

    // Reading file
    // Y
    if (getline (seqBCfile, sY))
    {
        seqBCfile.close();
    }
    else
    {
        std::cout << "Unable to open file seqBCfile" << std::endl;
        return -1;
    }


    // String to Vector of uint32_t
    std::stringstream ssY(sY);
    std::vector<uint32_t> Y, Y5;

    uint32_t number;

    while (ssY >> number)
    {
        Y.push_back(number);
    }


    uint32_t yDiff = 0;
    Y5.push_back(yDiff);
    // Read Y backwards
    for (std::vector<uint32_t>::reverse_iterator yLast = Y.rbegin(); yLast != Y.rend(); ++yLast) {
        if(Y.rend()-1 == yLast)
        {
            break;
        }
        yDiff += *yLast - *(yLast + 1);
        Y5.push_back(yDiff);
    }




    // Output files
    std::ofstream Y5file(path + ".seqY5", std::ofstream::out | std::ofstream::trunc);
    std::ofstream Y5fileBin(path + ".seqY5.bin", std::ofstream::binary | std::ofstream::trunc);

    // Write Y5 files, string and binary
    writeFileandBinary(Y5, Y5file, Y5fileBin);


    std::cout << Y.size() << " " << Y5.size() << std::endl;


    if (argv[2])
    {
        std::cout << "Y ";
        for (std::vector<unsigned int>::iterator i = Y.begin(); i != Y.end(); ++i)
        {
            std::cout << *i << " ";
        }
        std::cout << std::endl << std::endl;

        std::cout << "Y5" << std::endl;
        for (std::vector<uint32_t>::iterator i = Y5.begin(); i != Y5.end(); ++i)
        {
            std::cout << *i << " ";
        }
        std::cout << std::endl;
    }



    return 0;
}
