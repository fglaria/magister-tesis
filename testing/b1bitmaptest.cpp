// g++ -std=c++11 b1btmaptest.cpp -o b1btmaptest -O3 -DNDEBUG

#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

int main(int argc, char const *argv[])
{
    if(2 > argc)
    {
        std::cerr << "Modo de uso: " << argv[0] << " RUTA" << std::endl;
        return -1;
    }

    // Path to files
    const std::string basePath(argv[1]);

    std::string sB1 = "0100110001110000";
    std::vector<uint8_t> B1v8;
    std::vector<uint32_t> B1v32;
    

    uint8_t number;
    while (sB1 >> number)
    {
        B1v8.push_back(number);
        B1v32.push_back(number);
    }

    size_t Blen = B1v8.size();


    std::ofstream b1v8File(basePath + ".B1.v8", std::ofstream::out | std::ofstream::trunc);
    std::ofstream b1v32File(basePath + ".B1.v32", std::ofstream::out | std::ofstream::trunc);

    for(uint32_t index = 0; index < Blen; ++index)
    {
        b1v8File << B1v8[index] << " ";
        b1v32File << B1v32[index] << " ";
    }
    b1v8File.close();
    b1v32File.close();


    return 0;
}